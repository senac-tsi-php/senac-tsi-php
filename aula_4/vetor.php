<?php

//Mostra detalhes do ambiente 
//phpinfo();

//echo "Seu IP é: " . $_SERVER['REMOTE_ADDR'];

echo "Seu IP é:  {$_SERVER['REMOTE_ADDR']}";

//Vetor com mais de uma dimensão

$alunos[0]['nome'] = 'Luiz Fernando';
$alunos[0]['bitbucket'] = 'https://bitbuck...';

//Outra forma
$alunos = array(0 => array(	'nome' => 'Luiz Fer...',
							'bitbucket' => 'https://bi.'),
				1 => array(	'nome' => 'Carlos...',
							'bitbucket' => 'https://bi.'));

var_dump($alunos);