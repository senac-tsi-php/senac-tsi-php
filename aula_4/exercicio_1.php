<?php

//Vetor
$alunos = array(0 => array(	'nome' => 'Luiz ',
							'bitbucket' => 'https://bi.'),
				1 => array(	'nome' => 'Carlos',
							'bitbucket' => 'https://bi.'),
				2 => array(	'nome' => 'Aline',
							'bitbucket' => 'https://bi.'),
				3 => array(	'nome' => 'Maria',
							'bitbucket' => 'https://bi.'),
										);

//var_dump($alunos);
echo '<table border="1">
		<thead>
		<th>
			Nome
		</th>
		<th>
			Bitbucket
		</th>
		</thead>';

for ($i = 0 ; $i < count($alunos) ; $i++) {

	echo "	<tr>
				<td>
					{$alunos[$i]['nome']}
				</td>
				<td>
					{$alunos[$i]['bitbucket']}
				</td>
			</tr>";
}

echo '</table><br>';

//Agora com o foreach :-)

echo '<table border="1">
		<thead>
		<th>
			Nome
		</th>
		<th>
			Bitbucket
		</th>
		</thead>';

foreach ($alunos as $ind => $linha) {
	
	echo "	<tr>
				<td>
					{$linha['nome']}
				</td>
				<td>
					{$linha['bitbucket']}
				</td>
			</tr>";	
}

echo '</table>';