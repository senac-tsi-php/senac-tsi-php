<?php

class Usuario {

	private $id;
	private $nome;
	private $email;
	private $senha;
	private $objDb;

	public function __construct(){

		$this->objDb = new mysqli(	
								'localhost',
								'root',
								'',
								'aula_php',
								3308);

	}

	public function setId (int $id) {
		$this->id = $id;
	}

	public function setNome (string $nome) {
		$this->nome = $nome;
	}

	public function setEmail (string $email) {
		$this->email = $email;
	}

	public function setSenha (string $senha) {
		$this->senha = password_hash( $senha,PASSWORD_DEFAULT);
	}

	public function listar (): array {
		
		$objStmt = $this->objDb->prepare('
							SELECT 
								nome, email 
							FROM 
								tb_usuario 
							WHERE 
								id = ?');

		$objStmt->bind_param('i', $this->id);						
		$objStmt->execute();			

		$objResult = $objStmt->get_result();

		return $objResult->fetch_assoc();

	}

	public function apagar(){

		$objStmt = $this->objDb->prepare('DELETE FROM tb_usuario 
			WHERE id = ?');

		$objStmt->bind_param('i',
							$this->id);

		return $objStmt->execute();

	}

	public function salvar(){

		$objStmt = $this->objDb->prepare('REPLACE INTO tb_usuario 
			(id, nome, email, senha)
			VALUES
			(?, ?, ?, ?)');

		$objStmt->bind_param('isss',
							$this->id,
							$this->nome,
							$this->email,
							$this->senha);

		if ( $objStmt->execute() ){
			return  true;
		} else {
			return false;
		}
	}

	public function __destruct(){
		
		unset($this->objDb);
	}
}